package app.controller;

import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.net.URL;

/**
 * controleur pour Credit.fxml
 */
public class CreditController extends GridPane {

    // static pour éviter que le garbage collector de détruise la belle musique
    private static MediaPlayer mediaPlayer;
    private static boolean isPlaying = false;

    /**
     * initialise les elements de la page (musique)
     */
    @FXML
    private void initialize() {
        if (!isPlaying) {
            URL resource = getClass().getResource("/music/credit.mp3");
            mediaPlayer = new MediaPlayer(new Media(resource.toString()));
            mediaPlayer.play();
            isPlaying = true;
        }

    }
}
