package app.controller;

import app.model.rules.Rule;
import app.model.rules.RulesController;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * controleur pour RulesCustomContent.fxml
 */
public class RulesCustomContentController extends GridPane {

    private static final String RULES_ANCHOR_FXML = "/view/RulesCustomContent.fxml";
    private static final String STYLE_CLASS_TEXT = "text";
    private static final String BUNDLE_LANGUAGE = "strings";
    private static final int PADDING_LABEL_CHECKBOX = 3;

    //Les filtres :
    ArrayList<FileChooser.ExtensionFilter> filters = new ArrayList<>();
    private List<CheckBox> surviveCheckBoxes = new ArrayList<CheckBox>();
    private List<CheckBox> bornCheckBoxes = new ArrayList<CheckBox>();

    private RulesController rulesController;
    private boolean isSavingOrLoadingNewRule = false;
    private boolean isLoadingRuleInCheckboxes = false;

    @FXML
    private ComboBox<Rule> ruleComboBox;
    @FXML
    private Button saveButton;


    /**
     * Constructeur
     *
     * @param rulesController le controleur de rules
     */
    public RulesCustomContentController(RulesController rulesController) {
        this.rulesController = rulesController;
        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_LANGUAGE);

        FXMLLoader loader = new FXMLLoader(getClass().getResource(RULES_ANCHOR_FXML),bundle);
        loader.setController(this);
        loader.setRoot(this);

        filters.add(new FileChooser.ExtensionFilter("TXT (*.txt)", "*.txt"));
        filters.add(new FileChooser.ExtensionFilter("XML (*.xml)", "*.xml"));
        filters.add(new FileChooser.ExtensionFilter("BIN (*.bin)", "*.bin"));


        try {
            loader.load();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * FXML action click sur load
     */
    @FXML
    private void loadClick() {
        FileChooser fc = new FileChooser();
        File file = fc.showOpenDialog(new Stage());
        fc.setTitle("Choisir le fichier à importer :");
        if (file == null) {
            //probleme
            System.out.println("Probleme à l'ouverture du fichier !");
        } else {
            //tout s'est bien passé!
            //System.out.println("[INFO] : Le fichier existe !");

            isSavingOrLoadingNewRule = true;
            Rule res = rulesController.loadRule(file.getPath());
            if (res != null) ruleComboBox.getItems().add(res);
            else System.out.println("PROBLEME newRule == null");
            ruleComboBox.setValue(res);
            isSavingOrLoadingNewRule = false;
        }


    }


    /**
     * FXML action click sur load
     * Elle permet d'enregistrer les paramètres de règles de l'utilisateur dans un fichier qu'il choisira à travers une fenêtre de dialogue.
     */
    @FXML
    private void saveClick() {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(filters);
        fc.setSelectedExtensionFilter(filters.get(filters.size() - 1));
        File file = fc.showSaveDialog(new Stage()); // verifier aussi ici
        if (file == null) {
            //ou abandon de la sauvegarde
            System.out.println("Probleme lors du choix de la sauvegarde");
        } else {
            isSavingOrLoadingNewRule = true;
            Rule res = rulesController.saveCustomRule(file.getName(), file.getPath());
            if (res != null) ruleComboBox.getItems().add(res);
            else System.out.println("PROBLEME newRule == null");
            ruleComboBox.setValue(res);
            isSavingOrLoadingNewRule = false;
        }
    }


    /**
     * checkBoxChanged est une fonction appelée dès qu'une Checkbox change dans la vue.
     */
    private void checkBoxChanged(CheckBox cb) {
        //si on est en train de sauvegarder une nouvelle regle, alors quand on assigne auto les checkboxes on veut pas que l'évenement s'appelle
        if (isSavingOrLoadingNewRule || isLoadingRuleInCheckboxes) {
            return;
        }
        //On met la Rule sur "Custom ..." si la personne change les checkBoxs alors qu'il est sur une règle pré existante
        if (ruleComboBox.getValue() != rulesController.getCustomRule()) {
            ruleComboBox.setValue(rulesController.getCustomRule());
        }
        if (cb.getId().substring(0, 1).equals("S")) {
            //Si la checkBox est une Survive, si
            if (cb.isSelected()) {
                rulesController.getCustomRule().addSurviveInteger(Integer.parseInt(cb.getId().substring(1)));
            } else {
                rulesController.getCustomRule().delSurviveInteger(Integer.parseInt(cb.getId().substring(1)));
            }
        } else if (cb.getId().substring(0, 1).equals("B")) {
            if (cb.isSelected()) {
                rulesController.getCustomRule().addBornInteger(Integer.parseInt(cb.getId().substring(1)));
            } else {
                rulesController.getCustomRule().delBornInteger(Integer.parseInt(cb.getId().substring(1)));
            }
        } else {
            //probleme de fou
            System.out.println("[ERROR] : mauvais code ! ");
        }
    }

    /**
     * PRE_CONDITIONS : il y a autant de checkBoxes dans la collection que de colonnes dans la grille.
     * Permet de déployer une collection de VBoxes dans une grid.
     */
    private void deployCheckboxesGrid(GridPane gp, List<VBox> vBoxes) {
        for (int i = 0; i < vBoxes.size(); i++) {
            //Assignation de la VBox à une colonne bien spécifique dans la grille
            GridPane.setColumnIndex(vBoxes.get(i), i);
            //ajout de la VBox dans la grille
            gp.getChildren().add(vBoxes.get(i));
        }
    }

    /**
     * Permet d'initializer une collections de VBoxs
     */
    private List<VBox> initCheckBoxes(Collection<CheckBox> checkboxes, String idLetter) {
        StringBuilder id = new StringBuilder(idLetter);
        List<VBox> resultats = new ArrayList<VBox>();
        VBox vb;
        for (int i = 0; i < 9; i++) {
            //Creation de la VBox
            vb = new VBox();
            vb.setAlignment(Pos.CENTER);
            //Creation de la checkBox et ajout du gestionnaire de click
            CheckBox c = new CheckBox();
            c.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                    BooleanProperty bp = (BooleanProperty) observableValue;
                    CheckBox cb = (CheckBox) bp.getBean();                       // Ajoute une reference à chaque changement .. pas terrible ..> garbage collector ?
                    checkBoxChanged(cb);
                }
            });

            //set le nouvel identifiant
            id.delete(1, 2);
            id.append(i);
            c.setId(id.toString());
            //creation du label sous la checkbox
            Label label = new Label();
            label.setPadding(new Insets(PADDING_LABEL_CHECKBOX));
            label.getStyleClass().add(STYLE_CLASS_TEXT);
            label.setText(Integer.toString(i));
            //ajout du label et de la checkbox dans la VBox
            vb.getChildren().addAll(c, label);
            resultats.add(vb);
            checkboxes.add(c);

        }
        return resultats;
    }

    /**
     * initialise les elements de la page
     */
    @FXML
    public void initialize() {
        GridPane gpSurvive = (GridPane) this.lookup("#gridPaneSurvive"); //La grille dans laquelle on place les checkbox de survie
        GridPane gpBorn = (GridPane) this.lookup("#gridPaneBorn"); //La grille dans laquelle on place les checkbox de naissance alias born
        List<VBox> vboxesSurvives = initCheckBoxes(surviveCheckBoxes, "S");
        List<VBox> vboxesBorn = initCheckBoxes(bornCheckBoxes, "B");
        deployCheckboxesGrid(gpSurvive, vboxesSurvives);
        deployCheckboxesGrid(gpBorn, vboxesBorn);

        /*
        for (Rule r : rulesController.getRules()) {
            ruleComboBox.getItems().add(r);

        }
        */
        //ruleComboBox.valueProperty().bind(rulesController.getRulesProperty());
        ruleComboBox.setItems(rulesController.getRulesProperty());
        ruleComboBox.valueProperty().addListener((ChangeListener) (observableValue, o, t1) -> actualizeRegleBox());
        //on met une règle de base au demarage du jeu
        if(ruleComboBox.getItems().size() > 0){
            ruleComboBox.setValue(ruleComboBox.getItems().get(ruleComboBox.getItems().size() - 1));
        }
        rulesController.changeActiveRule(ruleComboBox.getValue());

        ruleComboBox.setCellFactory(new Callback<ListView<Rule>, ListCell<Rule>>() {
            @Override
            public ListCell<Rule> call(ListView<Rule> p) {
                return new ListCell<>() {
                    @Override
                    protected void updateItem(Rule item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null && !empty) {
                            setTextFill(Paint.valueOf("black"));
                            setText(item.getTitre());
                        }
                    }
                };
            }
        });

    }

    /**
     * actualizeRegleBox est appelée à chaque fois que la valeur de la regleBox est modifiée
     */
    private void actualizeRegleBox() {
        isLoadingRuleInCheckboxes = true;
        Collection<Integer> bornSet = ruleComboBox.getValue().getBornSet();
        for (int i = 0; i < 9; i++) {
            //actualisation du BornSet
            if (bornSet.contains(i)) {
                bornCheckBoxes.get(i).setSelected(true);
            } else {
                bornCheckBoxes.get(i).setSelected(false);
            }
        }
        Collection<Integer> surviveSet = ruleComboBox.getValue().getSurviveSet();
        for (int i = 0; i < 9; i++) {
            //actualisation du SurviveSet
            if (surviveSet.contains(i)) surviveCheckBoxes.get(i).setSelected(true);
            else surviveCheckBoxes.get(i).setSelected(false);
        }
        rulesController.changeActiveRule(ruleComboBox.getValue());
        isLoadingRuleInCheckboxes = false;
    }

}
