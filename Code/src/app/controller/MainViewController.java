package app.controller;

import app.model.Game;
import javafx.beans.binding.Bindings;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * controleur pour MainView.fxml
 */
public class MainViewController {

    private static final String MAIN_VIEW_FXML_PATH = "/view/MainView.fxml";
    private static final String CREDIT_ANCHOR_FXML = "/view/Credit.fxml";
    private static final String STAGE_CREDIT_TITLE = "Credit";
    private static final String STAGE_TITLE = "Game Of Life";
    private static final String ICON_PATH = "image/icon.png";
    private static final String BUNDLE_LANGUAGE = "strings";

    @FXML
    private Label cellPercentAliveLabel;
    @FXML
    private Label iterationLabel;
    @FXML
    private GridPane mainGridPain;
    @FXML
    private GridPane rightGridPane;
    @FXML
    private Button playButton;
    @FXML
    private CustomizeCustomContentController customizeContent;


    private Game game;
    private Stage stage;

    /**
     * constructeur
     *
     * @param stage stage où se charger
     * @param game  game principale
     */
    public MainViewController(Stage stage, Game game) {
        this.game = game;
        this.stage = stage;
        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_LANGUAGE);

        FXMLLoader loader = new FXMLLoader(getClass().getResource(MAIN_VIEW_FXML_PATH),bundle);
        loader.setController(this);

        stage.setTitle(STAGE_TITLE);
        stage.getIcons().add(new Image(ICON_PATH));

        try {
            GridPane root = loader.load();
            stage.setScene(new Scene(root));

        } catch (IOException e) {
            e.printStackTrace();
        }
        RulesCustomContentController contentController = new RulesCustomContentController(game.getRulesController());
        rightGridPane.add(contentController, 0, 2);


    }


    /**
     * FXML action click sur credit
     */
    @FXML
    private void creditClicked() {
        try {
            Stage stage = new Stage();
            stage.setScene(new Scene(FXMLLoader.load(getClass().getResource(CREDIT_ANCHOR_FXML))));
            stage.setTitle(STAGE_CREDIT_TITLE);
            stage.getIcons().add(new Image(ICON_PATH));
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * initialise les elements de la page
     */
    @FXML
    private void initialize() {
        // If mouse is dragging
        EventHandler<MouseEvent> mouseDragHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                //System.out.println("Mouse moved ! ");
                int nbCols = game.getTable().getCols();
                int nbRows = game.getTable().getRows();
                double colRealSize = game.getRenderer().getMyCanvas().getWidth();
                double rowRealSize = game.getRenderer().getMyCanvas().getHeight();
                double mouseX = e.getX();
                double mouseY = e.getY();

                int caseX = (int) (mouseX / colRealSize * nbCols);
                int caseY = (int) (mouseY / rowRealSize * nbRows);
                if (e.isPrimaryButtonDown()) {


                    for (int i =(int)(-customizeContent.brushSizeProperty().doubleValue() / 2) ; i < (int)(customizeContent.brushSizeProperty().doubleValue() / 2); i++) {
                        for (int j =(int)(-customizeContent.brushSizeProperty().doubleValue() / 2) ; j < (int)(customizeContent.brushSizeProperty().doubleValue() / 2); j++) {
                            game.getTable().getCell(caseY + i, caseX + j).setState(true);
                        }
                    }

                } else {
                    for (int i =(int)(-customizeContent.brushSizeProperty().doubleValue() / 2) ; i < (int)(customizeContent.brushSizeProperty().doubleValue() / 2); i++) {
                        for (int j =(int)(-customizeContent.brushSizeProperty().doubleValue() / 2) ; j < (int)(customizeContent.brushSizeProperty().doubleValue() / 2); j++) {
                            game.getTable().getCell(caseY + i, caseX + j).setState(false);
                        }
                    }
                }
                //System.out.println("[CASE] " + mouseX + " " + colRealSize + " " + nbCols + " " + " ");
                //System.out.println("[TAB] : "+caseX+" : "+caseY+"  :/:  "+e.getY()+" : "+rowRealSize + " : "+nbRows);
            }
        };


        if (game != null) {
            mainGridPain.getChildren().add(0, game.getRenderer().getMyCanvas());
            game.getRenderer().getMyCanvas().setOnMouseDragged(mouseDragHandler);
        }

        cellPercentAliveLabel.textProperty().bind(Bindings.format(ResourceBundle.getBundle(BUNDLE_LANGUAGE).getString("percent")+"%.2f", game.getTable().cellPercentAliveProperty()));
        iterationLabel.textProperty().bind(Bindings.format(ResourceBundle.getBundle(BUNDLE_LANGUAGE).getString("iteration")+"%d", game.getTable().iterationProperty()));

        playButton.textProperty().bind(Bindings.createStringBinding(() -> {
            String s = " ";
            if (game.getRenderer().getRunningState())
                s = ResourceBundle.getBundle(BUNDLE_LANGUAGE).getString("pause");
            else
                s = ResourceBundle.getBundle(BUNDLE_LANGUAGE).getString("play");

            return s;
        }, game.getRenderer().runningStateProperty()));

        customizeContent.setGame(game);
        customizeContent.loadSpinner();

        Bindings.bindBidirectional(customizeContent.frameRateProperty(), game.getRenderer().frameRateProperty());

        Bindings.bindBidirectional(customizeContent.aliveColorProperty(), game.getRenderer().getMyCanvas().aliveColorProperty());
        Bindings.bindBidirectional(customizeContent.deadColorProperty(), game.getRenderer().getMyCanvas().deadColorProperty());
        Bindings.bindBidirectional(customizeContent.gridCellColorProperty(), game.getRenderer().getMyCanvas().gridColorProperty());

    }

    /**
     * FXML action click sur play
     */
    @FXML
    private void playClicked() {
        game.getRenderer().changeStateRunning();
    }


    /**
     * FXML action click sur oneFrameBack
     */
    @FXML
    private void oneFrameBackClicked(MouseEvent mouseEvent) {
        game.getRenderer().previousFrame();
    }

    /**
     * FXML action click sur oneFrameForw
     */
    @FXML
    private void oneFrameForwClicked(MouseEvent mouseEvent) {
        game.getRenderer().nextFrame();
    }

    /**
     * FXML action click sur reset
     */
    @FXML
    private void resetClicked(MouseEvent mouseEvent) {
        game.getRenderer().clearArea();
    }

    /**
     * FXML action click sur random
     */
    @FXML
    private void randomClicked(MouseEvent mouseEvent) {
        game.getTable().random();
    }

    /**
     * FXML action click sur screenshot
     */
    @FXML
    private void screenshotClicked(MouseEvent mouseEvent) {


        WritableImage writableImage = new WritableImage((int) game.getRenderer().getMyCanvas().getWidth(), (int) game.getRenderer().getMyCanvas().getHeight());
        game.getRenderer().getMyCanvas().snapshot(null, writableImage);
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);

        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("png (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(filter);

        File file = fileChooser.showSaveDialog(stage.getOwner());


        if (file != null) {
            try {

                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
                System.out.println("Impossible d'enregistrer la screenshot");
            }
        }
    }

}
