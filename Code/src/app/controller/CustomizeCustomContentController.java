package app.controller;

import app.model.Game;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * controleur pour CustomizeCustomContent.fxml
 */
public class CustomizeCustomContentController extends GridPane {
    private static final String RULES_ANCHOR_FXML = "/view/CustomizeCustomContent.fxml";
    private static final String BUNDLE_LANGUAGE = "strings";

    private Game game;

    @FXML
    private ColorPicker gridCellColorPicker;
    @FXML
    private ColorPicker aliveCellColorPicker;
    @FXML
    private ColorPicker deadCellColorPicker;
    @FXML
    private Spinner<Integer> spinnerRows;
    @FXML
    private Spinner<Integer> spinnerCols;
    @FXML
    private Slider frameRateSlider;
    @FXML
    private Slider brushLabelSlider;
    @FXML
    private Label fpsLabel;
    @FXML
    private Label brushLabel;

    private Random random;


    /**
     * constructeur
     */
    public CustomizeCustomContentController() {
        random = new Random();

        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_LANGUAGE);
        FXMLLoader loader = new FXMLLoader(getClass().getResource(RULES_ANCHOR_FXML),bundle);
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Permet d'exposer la propriété color du colorPicker aliveCellColorPicker
    public ObjectProperty<Color> aliveColorProperty() {
        return aliveCellColorPicker.valueProperty();
    }

    //Permet d'exposer la propriété color du colorPicker deadCellColorPicker
    public ObjectProperty<Color> deadColorProperty() {
        return deadCellColorPicker.valueProperty();
    }

    //Permet d'exposer la propriété color du colorPicker gridCellColorPicker
    public ObjectProperty<Color> gridCellColorProperty() {
        return gridCellColorPicker.valueProperty();
    }

    //Permet d'exposer la propriété value du frameRateSlider
    public DoubleProperty frameRateProperty() {
        return frameRateSlider.valueProperty();
    }

    public DoubleProperty brushSizeProperty(){
        return brushLabelSlider.valueProperty();
    }

    public Color getAliveColor() {
        return aliveCellColorPicker.getValue();
    }

    public void setAliveColor(Color value) {
        aliveCellColorPicker.setValue(value);
    }

    public Color getDeadColor() {
        return deadCellColorPicker.getValue();
    }

    public void setDeadColor(Color value) {
        aliveCellColorPicker.setValue(value);
    }

    public Color getGridColor() {
        return gridCellColorPicker.getValue();
    }

    public void setGridColor(Color value) {
        gridCellColorPicker.setValue(value);
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * initialise les elements de la page
     */
    @FXML
    private void initialize() {


        fpsLabel.textProperty().bind(Bindings.format(ResourceBundle.getBundle("strings").getString("fps")+" %.1f", frameRateSlider.valueProperty()));
        brushLabel.textProperty().bind(Bindings.format(ResourceBundle.getBundle("strings").getString("brush_size")+" %.1f", brushLabelSlider.valueProperty()));
        gridCellColorPicker.setValue(Color.GRAY);
        aliveCellColorPicker.setValue(Color.BLACK);
        deadCellColorPicker.setValue(Color.WHITE);
    }

    /**
     * charge les spinners de la page
     */
    public void loadSpinner() {
        Bindings.bindBidirectional(spinnerRows.getValueFactory().valueProperty(), game.getTable().rowsProperty());
        Bindings.bindBidirectional(spinnerCols.getValueFactory().valueProperty(), game.getTable().colsProperty());
    }


    /**
     * FXML action click sur loadGrid
     */
    @FXML
    private void loadGridOnClick() {
        game.getRenderer().stopRunning();
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("RLE (*.rle)", "*.rle"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("GOL (*.gol)", "*.gol"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("BMP (*.bmp)", "*.bmp"));
        File file = fc.showOpenDialog(new Stage());
        game.getTableManager().load(file, game.getTable());
    }

    /**
     * FXML action click sur saveGrid
     */
    @FXML
    private void saveGridOnClick() {
        game.getRenderer().stopRunning();
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("GOL (*.gol)", "*.gol"));
        File file = fc.showSaveDialog(new Stage());
        game.getTableManager().save(file, game.getTable());
    }


    /**
     * FXML action click sur saveGrid
     */
    @FXML
    private void RGBCLicked() {
        game.getRenderer().changeRgbMode();

    }

}
