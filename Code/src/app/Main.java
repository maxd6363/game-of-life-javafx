package app;

import app.controller.MainViewController;
import app.model.Game;
import app.model.rules.RulesController;
import app.model.serializationRule.ruleLoader.RuleLoaderBin;
import app.model.serializationRule.ruleSave.RuleSaverBin;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 */
public class Main extends Application {

    private static int WINDOW_WIDTH = 1280;
    private static int WINDOW_HEIGHT = 720;

    private Game game;
    private RulesController rulesController;
    private MainViewController mainViewController;


    /**
     * Point d'entrée de l'application
     *
     * @param args args to pass to the app
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * initialise l'application
     *
     * @throws Exception any exception threw by the app
     */
    @Override
    public void init() throws Exception {
        super.init();
    }

    /**
     * demarre l'application
     *
     * @param stage stage où l'application se lance
     * @throws Exception any exception threw by the app
     */
    @Override
    public void start(Stage stage) throws Exception {
        rulesController = new RulesController(new RuleLoaderBin(), new RuleSaverBin());
        game = new Game(rulesController);
        game.launch();

        mainViewController = new MainViewController(stage, game);


        stage.setWidth(WINDOW_WIDTH);
        stage.setHeight(WINDOW_HEIGHT);

        stage.show();

    }
}
