package app.model.rules;

import java.io.Serializable;
import java.util.TreeSet;

/**
 * classe représentant une règle
 */
public class Rule implements Serializable {
    private Integer identifiant; // est-ce necessaire ?
    private String titre;
    private TreeSet<Integer> bornSet;
    private TreeSet<Integer> surviveSet;
    private String extension;
    private String pathToRule;

    /**
     * constructeur
     * @param titre titre de la règle
     */
    public Rule(String titre) {
        this.identifiant = -1;
        this.titre = titre;
        this.surviveSet = new TreeSet<Integer>();
        this.bornSet = new TreeSet<Integer>();
    }

    /**
     * constructeur
     * @param r règle de base
     */
    public Rule(Rule r) {
        this.identifiant = r.getIdentifiant();
        this.bornSet = new TreeSet<Integer>(r.getBornSet());
        this.surviveSet = new TreeSet<Integer>(r.getSurviveSet());

    }


    /**
     * constructeur
     * @param identifier id de la règle
     * @param titre titre de la règle
     * @param born treeSet pour le nombre de voisin permettant une naissance de la cellule
     * @param alive treeSet pour le nombre de voisin causant la survie de la cellule
     */
    public Rule(int identifier, String titre, TreeSet<Integer> born, TreeSet<Integer> alive) {
        this.identifiant = identifier;
        this.titre = titre;
        this.surviveSet = alive;
        this.bornSet = born;
    }

    public String getTitre() {
        return titre;
    }

    public TreeSet<Integer> getBornSet() {
        return bornSet;
    }

    public TreeSet<Integer> getSurviveSet() {
        return surviveSet;
    }

    public Integer getIdentifiant() {
        return identifiant;
    }

    /**
     * ajoute un nombre de voisin pour la naissance d'une cellule
     * @param value nombre de voisin
     * @return resultat de l'ajout
     */
    public boolean addBornInteger(int value) {
        if (!bornSet.contains(value)) {
            bornSet.add(value);
            return true;
        }
        return false;
    }

    /**
     * ajoute un nombre de voisin pour la survie d'une cellule
     * @param value nombre de voisin
     * @return resultat de l'ajout
     */
    public boolean addSurviveInteger(int value) {
        if (!surviveSet.contains(value)) {
            surviveSet.add(value);
            return true;
        }
        return false;
    }

    /**
     * supprime un nombre de voisin pour la naissance d'une cellule
     * @param value nombre de voisin
     * @return resultat de la suppression
     */
    public boolean delBornInteger(int value) {
        if (bornSet.contains(value)) {
            bornSet.remove(value);
            return true;
        }
        return false;
    }
    /**
     * supprime un nombre de voisin pour la survie d'une cellule
     * @param value nombre de voisin
     * @return resultat de la suppression
     */
    public boolean delSurviveInteger(int value) {
        if (surviveSet.contains(value)) {
            surviveSet.remove(value);
            return true;
        }
        return false;
    }

    public boolean bornContains(int value) {
        return bornSet.contains(value);
    }

    public boolean surviveContains(int value) {
        return surviveSet.contains(value);
    }

    public void setRulePath(String value) {
        this.pathToRule = value;
    }

    public String getRulePath() {
        return this.pathToRule;
    }

    public void setTitre(String value) {
        this.titre = value;
    }

    @Override
    public String toString() {
        return this.getTitre();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != this.getClass()) return false;
        if (o == this) return true;
        Rule r = (Rule) o;
        return r.titre.equals(this.titre) && r.getRulePath().equals(this.pathToRule);

    }
}
