package app.model.rules;

import app.model.PropertiesRegistre;
import app.model.serializationRule.ruleLoader.RuleLoader;
import app.model.serializationRule.ruleSave.RuleSaver;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.util.*;

/**
 * Cette classe permet de gérer les règles dans le jeu, elle les stock, les sauvegarde, et les chargent.
 */
public final class RulesController {
    /**
     * Permet de connaitre et de stocker le nom de la règle "customizable" celle qui n'est pas enregistré.
     */
    public static final String CUSTOM_RULE_NAME = "Custom ...";
    private RuleSaver saver;
    private RuleLoader loader;
    private Map<Integer, Rule> rules;

    private ObservableList<Rule> Obs_rules;
    private Rule customRule;
    private Rule activeRule;
    private PropertiesRegistre ruleRegistre;
    public SimpleListProperty<Rule> rulesProperty;
    /**
     * Constructeur de RulesController.
     *
     * @param l : Le Loader de Règles
     * @param s : Le saver de Règles
     */
    public RulesController(RuleLoader l, RuleSaver s) {
        this.loader = l;
        this.saver = s;
        this.rulesProperty = new SimpleListProperty<Rule>(FXCollections.observableList(new LinkedList<>()));
        this.ruleRegistre = new PropertiesRegistre();
        this.customRule = new Rule(CUSTOM_RULE_NAME);
        this.activeRule = customRule;
        initRules();

    }

    public SimpleListProperty<Rule> getRulesProperty(){return rulesProperty;}


    /**
     * Permet de changer la règle active avec celle passé en paramètre
     *
     * @param r : La nouvelle règle active
     */
    public void changeActiveRule(Rule r) {
        this.activeRule = (r != null) ? r : customRule;
    }

    /**
     * Permet d'initialiser toutes les regles qui sont dans le registre
     */
    private void initRules() {
        ruleRegistre.actualizeRegister(); // mets à jour le registre pour supprimer les règles qui n'existent plus
        rulesProperty.addAll(loader.loadAll(ruleRegistre.getRulePathsInRegister())); //Load depuis les paths spécifiés

        rulesProperty.add(customRule);
        loadStubRules();
        System.out.println("init rules : " + rulesProperty.getValue().toString());
    }

    /**
     * Permet de sauvegarder la règle custom actuelle, va réinitialiser la règle custom à vide.
     */
    public Rule saveCustomRule(String title, String path) {
        Rule newRule = new Rule(customRule);
        newRule.setRulePath(path);
        newRule.setTitre(title);
        boolean ok = ajouterUneRegle(newRule);
        customRule = new Rule(CUSTOM_RULE_NAME);
        return (ok) ? newRule : null;
    }

    /**
     * Permet de charger une règle depuis un chemin d'accès.
     *
     * @param path : Le chemin d'accès de la règle concernée
     * @return : Une règle résultant du fichier correspondant au chemin d'accès.
     */
    public Rule loadRule(String path) {
        Rule tmp = loader.load(path);
        if (tmp != null) {
            ruleRegistre.saveOneRuleToRegistre(tmp);
        } else {
            return null;
        }
        return tmp;
    }

    /**
     * Permet d'ajouter une règle dans la liste de règles.
     *
     * @param r : La règle à ajouter
     * @return : true si tout s'est bien passé, false sinon
     */
    private boolean ajouterUneRegle(Rule r) {
        if (!rulesProperty.contains(r)) {
            try {
                boolean ok = saver.save(r); // lorsque le saver sera implémenté
                //rules.put(rules.size(), r);
                rulesProperty.add(r);
                if (ok) ruleRegistre.saveRulesToRegistre(rulesProperty.getValue());
                return ok;
            } catch (Exception e) {
                //GESTION DES ERREURS ICI ===========================
                return false;
            }
        }
        return false;
    }

    /**
     * Permet de supprimer une règle de la liste de règles.
     *
     * @param r : la règle à supprimer
     * @return : true si la règle a bien été suprimée, false sinon
     */
    public boolean supprimerUneRegle(Rule r) {
        if (rulesProperty.contains(r)) {
            try {
                File file = new File(r.getRulePath());
                if (file.isDirectory()) {
                    System.out.println("[ERROR] : le chemin d'acces de la rule identifie un répertoire !");
                    return false;
                } else {
                    //supprime de la liste de regles dans le controlleur
                    System.out.println(r.getTitre() + " a été supprimé !");
                    //supprimer le fichier sur l'ordinateur
                    boolean ok = file.delete();
                    //on le supprime du registre de regles
                    if (ok) ruleRegistre.saveRulesToRegistre(rulesProperty.getValue());
                    return ok;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /**
     * Permet de savoir si une regle existe déjà ou non dans la mémoire.
     *
     * @param r : La règle à tester.
     * @return : true si la règle existe, false sinon
     */
    public boolean isRuleExisting(Rule r) {
        boolean ok = false;
        if (r == null) return false;
        try {
            File ruleTmp = new File(r.getRulePath());
            ok = ruleTmp.exists();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return rulesProperty.contains(r) && ok;
    }

    /**
     * Permet de savoir si la Rule r est dans la liste de rules.
     *
     * @param r : la rule à rechercher.
     * @return true si elle est dans la liste, false sinon
     */
    public boolean isRuleInRulesList(Rule r) {
        return rules.containsValue(r);
    }

    /**
     * Permet de récupérer la liste de règles.
     *
     * @return : la liste de règles
     */
    public Collection<Rule> getRules() {
        return rules.values();
    }

    /**
     * Permet de récupérer la Règle custom. Celle qui est éditable.
     *
     * @return : la régle custom
     */
    public Rule getCustomRule() {
        return customRule;
    }

    /**
     * Permet de récupérer la règle active
     *
     * @return : La règle active
     */
    public Rule getActiveRule() {
        return activeRule;
    }

    /**
     * Charge les règles du stub
     */
    private void loadStubRules() {
        rulesProperty.add(new Rule(rulesProperty.size(), "2 X 2", new TreeSet<>(Arrays.asList(3, 6)), new TreeSet<>(Arrays.asList(1, 2, 5))));
        rulesProperty.add(new Rule(rulesProperty.size(), "34Life", new TreeSet<>(Arrays.asList(3, 4)), new TreeSet<>(Arrays.asList(3, 4))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Amoeba", new TreeSet<>(Arrays.asList(3, 5, 7)), new TreeSet<>(Arrays.asList(1, 3, 5, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Assimilation", new TreeSet<>(Arrays.asList(3, 4, 5)), new TreeSet<>(Arrays.asList(4, 5, 6, 7))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Coagulation", new TreeSet<>(Arrays.asList(3, 7, 8)), new TreeSet<>(Arrays.asList(2, 3, 6, 7, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Coral", new TreeSet<>(Collections.singletonList(3)), new TreeSet<>(Arrays.asList(4, 5, 6, 7, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Day & Night", new TreeSet<>(Arrays.asList(3, 6, 7, 8)), new TreeSet<>(Arrays.asList(3, 4, 6, 7, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Diamoeba", new TreeSet<>(Arrays.asList(3, 5, 7, 8)), new TreeSet<>(Arrays.asList(5, 6, 7, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Flakes ", new TreeSet<>(Collections.singletonList(3)), new TreeSet<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Gnarl", new TreeSet<>(Collections.singletonList(1)), new TreeSet<>(Collections.singletonList(1))));
        rulesProperty.add(new Rule(rulesProperty.size(), "High Life", new TreeSet<>(Arrays.asList(3, 6)), new TreeSet<>(Arrays.asList(2, 3))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Inverse Life", new TreeSet<>(Arrays.asList(0, 1, 2, 3, 4, 7, 8)), new TreeSet<>(Arrays.asList(3, 4, 6, 7, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Long Life", new TreeSet<>(Arrays.asList(3, 4, 5)), new TreeSet<>(Collections.singletonList(5))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Maze", new TreeSet<>(Collections.singletonList(3)), new TreeSet<>(Arrays.asList(1, 2, 3, 4, 5))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Mazectric", new TreeSet<>(Collections.singletonList(3)), new TreeSet<>(Arrays.asList(1, 2, 3, 4))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Move", new TreeSet<>(Arrays.asList(3, 6, 8)), new TreeSet<>(Arrays.asList(2, 4, 5))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Pseudo Life", new TreeSet<>(Arrays.asList(3, 5, 7)), new TreeSet<>(Arrays.asList(2, 3, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Replicator", new TreeSet<>(Arrays.asList(1, 3, 5, 7)), new TreeSet<>(Arrays.asList(1, 3, 5, 7))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Seeds", new TreeSet<>(Collections.singletonList(2)), new TreeSet<>(Collections.emptyList())));
        rulesProperty.add(new Rule(rulesProperty.size(), "Serviettes", new TreeSet<>(Arrays.asList(2, 3, 4)), new TreeSet<>(Collections.emptyList())));
        rulesProperty.add(new Rule(rulesProperty.size(), "Stains", new TreeSet<>(Arrays.asList(3, 6, 7, 8)), new TreeSet<>(Arrays.asList(2, 3, 5, 6, 7, 8))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Walled Cities", new TreeSet<>(Arrays.asList(4, 5, 6, 7, 8)), new TreeSet<>(Arrays.asList(2, 3, 4, 5))));
        rulesProperty.add(new Rule(rulesProperty.size(), "Conway's Life", new TreeSet<>(Collections.singletonList(3)), new TreeSet<>(Arrays.asList(2, 3))));

    }
}
