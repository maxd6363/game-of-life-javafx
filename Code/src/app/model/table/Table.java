package app.model.table;

import app.model.cell.Cell;
import app.model.rules.RulesController;
import javafx.beans.property.*;

import java.io.Serializable;

public abstract class Table implements Serializable {

    protected Cell[][] cells;
    protected RulesController rulesController;

    protected Property<Integer> rows;
    protected Property<Integer> cols;

    private LongProperty iteration;
    private FloatProperty cellPercentAlive;

    /**
     * Constructeur de Table.
     *
     * @param rulesController : Le controleur qui gère les règles du jeu.
     */
    public Table(RulesController rulesController) {



        this.rulesController = rulesController;
        cellPercentAlive = new SimpleFloatProperty();
        iteration = new SimpleLongProperty(0);


    }

    public Property<Integer> rowsProperty() {
        return rows;
    }

    public int getRows() {
        return rows.getValue();
    }

    public void setRows(int value) {
        rows.setValue(value);
    }

    public Property<Integer> colsProperty() {
        return cols;
    }

    public int getCols() {
        return cols.getValue();
    }

    public void setCols(int value) {
        cols.setValue(value);
    }

    public LongProperty iterationProperty() {
        return iteration;
    }

    public long getIteration() {
        return iteration.get();
    }

    public void setIteration(long value) {
        this.iteration.set(value);
    }

    public FloatProperty cellPercentAliveProperty() {
        return cellPercentAlive;
    }

    public float getCellPercentAlive() {
        return cellPercentAlive.get();
    }

    private void setCellPercentAlive(float value) {
        cellPercentAlive.set(value);
    }

    /**
     * Permet de récupérer l'état des cellules sous la forme d'un tableau de boolean à 2 dimensions.
     *
     * @return Le tableau de boolean correspondant à l'état des cellules.
     */
    public boolean[][] getCells() {
        boolean[][] tmp = new boolean[getRows()][getCols()];
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                tmp[i][j] = cells[i][j].getState();
            }
        }
        return tmp;
    }

    /**
     * Permet de initialiser les états des cells dans le tableau de cells.
     *
     * @param cells : un tableau de Boolean à 2 dimensions qui représente l'état de chaque cellules.
     */
    public void setCells(boolean[][] cells) {
        int max_X = Math.min(this.cells.length, cells.length);
        int max_Y = Math.min(this.cells[0].length, cells[0].length);
        for (int i = 0; i < max_X; i++) {
            for (int j = 0; j < max_Y; j++) {
                this.cells[i][j].setState(cells[i][j]);
            }
        }
    }

    /**
     * Permet de lancer la génération de la prochaine frame. Cette fonction va appliquer la règle en cours sur la Table en cours, pour en déterminer une table résultante.
     */
    public void processOneGeneration() {
        calculatePercentAlive();
        setIteration(getIteration() + 1);
    }

    public abstract void random();

    protected abstract void randomGen(float percentOfCellsToSetAlive);

    public abstract void killAllCells();


    protected int getCellAsInt(int x, int y) {
        return getCell(x, y).getStateAsInt();
    }


    public int getNumberOfCells() {
        return getCols() * getRows();
    }


    public Cell getCell(int x, int y) {
        if (x < 0) x = getRows() - Math.abs(x);
        if (y < 0) y = getCols() - Math.abs(y);

        return cells[x % getRows()][y % getCols()];
    }


    public void setCell(int x, int y, boolean state) {
        if (x < 0) x = getRows() - Math.abs(x);
        if (y < 0) y = getCols() - Math.abs(y);
        cells[x % getRows()][y % getCols()].setState(state);
    }


    protected int getNeighborhood(int x, int y) {
        return getCellAsInt(x - 1, y - 1) + getCellAsInt(x - 1, y) + getCellAsInt(x - 1, y + 1) + getCellAsInt(x, y - 1) + getCellAsInt(x, y + 1) + getCellAsInt(x + 1, y - 1) + getCellAsInt(x + 1, y) + getCellAsInt(x + 1, y + 1);
    }

    private void calculatePercentAlive() {
        int cellAlive = 0;
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                if (getCell(i, j).getState()) {
                    cellAlive++;
                }
            }
        }
        setCellPercentAlive(cellAlive * 100.0f / (getRows() * getCols()));
    }


    @Override
    public String toString() {

        StringBuilder str = new StringBuilder();
        str.append("Table : ").append(getRows()).append(" x ").append(getCols()).append("\n");

        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                str.append(getCellAsInt(i, j)).append(" ");
            }
            str.append("\n");
        }

        return str.toString();

    }
}
