package app.model.table;

import app.model.cell.Cell;
import app.model.cell.SquareCell;
import app.model.rules.RulesController;
import app.model.utilities.Utils;
import javafx.beans.property.SimpleObjectProperty;

public class OrthogonalTable extends Table {

    private final static float PERCENT_RANDOM = 0.5f;


    /**
     * Constructeur de OrthogonalTable
     *
     * @param rulesController : Le controleur qui gère les règles.
     * @param rows            : le nombre de lignes de base
     * @param cols            : Le nombre de colonnes de base
     */

    public OrthogonalTable(RulesController rulesController, int rows, int cols) {
        super(rulesController);
        if (rows <= 0) rows = 1;
        if (cols <= 0) cols = 1;

        this.rows = new SimpleObjectProperty<>(this, "rows", rows) {
            /**
             * Custom setter pour la propriété
             */
            @Override
            public void set(Integer integer) {
                if (integer != getRows()) {
                    super.set(integer);
                    setRows(integer);
                }
            }
        };

        this.cols = new SimpleObjectProperty<>(this, "cols", cols) {
            /**
             * Custom setter pour la propriété
             */
            @Override
            public void set(Integer integer) {
                if (integer != getCols()) {
                    super.set(integer);
                    setCols(integer);
                }
            }
        };

        super.setCols(cols);
        super.setRows(rows);

        cells = new Cell[getRows()][getCols()];
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                cells[i][j] = new SquareCell();
            }
        }
    }

    /**
     * Permet de changer le nombre de lignes que possède la table.
     *
     * @param value : Le nouveau nombre de lignes que doit comporter la table.
     */
    @Override
    public void setRows(int value) {
        int deltaRows = value - cells.length;
        if(deltaRows < 0) deltaRows = 0;
        super.setRows(value);
        Cell[][] aux = new Cell[getRows()][getCols()];
        for (int i = 0; i <= deltaRows; i++) {
            for (int j = 0; j < getCols(); j++) {
                aux[i][j] = new SquareCell();
            }
        }

        for (int i = deltaRows + 1; i < getRows()-deltaRows; i++) {
            for (int j = 0; j < getCols(); j++) {
                try {
                    aux[i][j] = cells[i][j];
                } catch (ArrayIndexOutOfBoundsException e) {
                    aux[i][j] = new SquareCell();
                }
            }
        }


        for (int i = getRows()-deltaRows; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                aux[i][j] = new SquareCell();
            }
        }

        cells = aux;
    }

    /**
     * Permet de changer le nombre de colonnes dans la table
     *
     * @param value : Le nouveau nombre de colonnes que doit comporter la table.
     */
    @Override
    public void setCols(int value) {
        int deltaCols = value - cells[1].length;
        if(deltaCols < 0) deltaCols = 0;
        super.setCols(value);
        Cell[][] aux = new Cell[getRows()][getCols()];
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j <= deltaCols; j++) {
                aux[i][j] = new SquareCell();
            }
        }

        for (int i = 0; i < getRows(); i++) {
            for (int j = deltaCols+1; j < getCols() - deltaCols; j++) {
                try {
                    aux[i][j] = cells[i][j];
                } catch (ArrayIndexOutOfBoundsException e) {
                    aux[i][j] = new SquareCell();
                }
            }
        }


        for (int i = 0; i < getRows(); i++) {
            for (int j = getCols() - deltaCols; j < getCols(); j++) {
                aux[i][j] = new SquareCell();
            }
        }
        cells = aux;
    }


    /**
     * Permet de générer automatiquement et de manière pseudo aléatoire une configuration de la Table.
     *
     * @param percentOfCellsToSetAlive : Le pourcentage de cellules qui devront être en vie après la génération.
     */
    @Override
    protected void randomGen(float percentOfCellsToSetAlive) {

        if (percentOfCellsToSetAlive > 1) percentOfCellsToSetAlive = 1;
        if (percentOfCellsToSetAlive < 0) percentOfCellsToSetAlive = 0;

        for (int i = 0; i < percentOfCellsToSetAlive * getNumberOfCells(); i++) {
            int x = (int) (Utils.getRandom().nextDouble() * getRows());
            int y = (int) (Utils.getRandom().nextDouble() * getCols());

            getCell(x, y).setState(true);
        }
    }

    /**
     * Permet de tuer toutes les cellules de la table.
     */
    @Override
    public void killAllCells() {
        setIteration(0);
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                getCell(i, j).setState(false);
            }
        }
    }


    /**
     * Permet de lancer la génération de la prochaine frame. Cette fonction va appliquer la règle en cours sur la Table en cours, pour en déterminer une table résultante.
     */
    @Override
    public void processOneGeneration() {
        super.processOneGeneration();
        int neighborhood;
        setOldStateAsCurrentState();
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                neighborhood = getNeighborhood(i, j);
                if (!getCell(i, j).getState()) {
                    if (rulesController.getActiveRule().getBornSet().contains(neighborhood)) {
                        getCell(i, j).setOldState(true);
                    }
                } else {
                    if (rulesController.getActiveRule().getSurviveSet().contains(neighborhood)) {
                        getCell(i, j).setOldState(true);
                    } else {
                        getCell(i, j).setOldState(false);
                    }
                }

            }
        }
        setCurrentStateAsOldState();
    }

    /**
     * Méthode publique qui permet de générer automatiquement et de manière pseudo aléatoire une configuration de la Table.
     */
    @Override
    public void random() {
        randomGen(PERCENT_RANDOM);
    }

    /**
     * Permet de set l'ancien état de la table comme état courant.
     */
    private void setOldStateAsCurrentState() {
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                getCell(i, j).setOldStateAsCurrentState();
            }
        }
    }

    /**
     * Permet de set l'état courant de la table en tant que ancien état.
     */
    private void setCurrentStateAsOldState() {
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                getCell(i, j).setCurrentStateAsOldState();
            }
        }
    }


}
