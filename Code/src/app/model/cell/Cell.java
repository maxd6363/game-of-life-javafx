package app.model.cell;

import java.io.Serializable;

/**
 * classe représentant une cellule du game of life
 */
public abstract class Cell implements Serializable {
    protected boolean state;
    protected boolean oldState;

    public boolean getState() {

        return state;
    }

    public void setState(boolean state) {

        this.state = state;
    }

    public boolean getOldState() {

        return oldState;
    }

    public void setOldState(boolean state) {
        oldState = state;
    }

    public void setOldStateAsCurrentState() {
        oldState = state;
    }

    public void setCurrentStateAsOldState() {
        state = oldState;
    }

    public int getStateAsInt() {
        return getState() ? 1 : 0;
    }

    @Override
    public String toString() {
        return Integer.toString(getStateAsInt());
    }


}
