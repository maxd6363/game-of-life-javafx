package app.model.serializationTable.tableLoader;

import app.model.table.Table;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * classe implémentant TableLoader, charge une table à partir d'un fichier RLE (format standart pour les tables Game Of Life)
 */
public class TableLoaderRLE implements TableLoader {
    /**
     * algorythme original pour décoder le format de fichier .RLE et le charger
     *
     * @param file  fichier à charger
     * @param table table à remplir
     */
    @Override
    public void load(File file, Table table) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            StringBuilder allLines = new StringBuilder();
            String line = bufferedReader.readLine();
            Scanner scanner;
            // remove comments
            while (line.getBytes()[0] == '#') {
                line = bufferedReader.readLine();
            }
            String[] firstLines = line.split("[,]");
            int lineCount = 0;
            int columnCount = 0;
            int rows = Integer.parseInt(firstLines[0].substring(4));
            int cols = Integer.parseInt(firstLines[1].substring(5));

            table.setRows((int) (rows * 1.5f));
            table.setCols((int) (cols * 1.5f));
            table.killAllCells();

            line = bufferedReader.readLine();
            while (line != null) {
                allLines.append(line);
                line = bufferedReader.readLine();
            }

            for (String s : allLines.toString().replaceAll("[!]", "").replaceAll("[\n]", "").split("[$]")) {
                columnCount = 0;
                for (char c : decode(s).toCharArray()) {
                    if (c == 'b') {
                        table.setCell(lineCount, columnCount, false);
                    } else {
                        table.setCell(lineCount, columnCount, true);
                    }
                    columnCount++;
                }
                lineCount++;
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * décode une ligne de RLE
     * @param string ligne à décoder
     * @return ligne décodé
     */
    public String decode(String string) {
        if (string == null || string.isEmpty()) return "";
        StringBuilder builder = new StringBuilder();
        char[] chars = string.toCharArray();
        boolean preIsDigit = false;
        StringBuilder digitString = new StringBuilder();
        for (char current : chars) {
            if (!Character.isDigit(current)) {
                if (preIsDigit) {
                    String multipleString = new String(new char[Integer.parseInt(digitString.toString())]).replace("\0", current + "");
                    builder.append(multipleString);
                    preIsDigit = false;
                    digitString = new StringBuilder();
                } else {
                    builder.append(current);
                }
            } else {
                digitString.append(current);
                preIsDigit = true;
            }
        }
        return builder.toString();
    }


}
