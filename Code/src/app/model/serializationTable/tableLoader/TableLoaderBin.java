package app.model.serializationTable.tableLoader;

import app.model.table.Table;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * classe implémentant TableLoader, charge une table à partir d'un fichier binaire
 */
public class TableLoaderBin implements TableLoader {

    /**
     * charge une table à partir d'un fichier
     * @param file ficher ou se trouve la table
     * @param table table à remplir
     */
    @Override
    public void load(File file, Table table) {
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream(file));
            int rows = dis.readInt();
            int cols = dis.readInt();
            boolean state;

            table.setRows(rows);
            table.setCols(cols);

            for (int i = 0; i < table.getRows(); i++) {
                for (int j = 0; j < table.getCols(); j++) {
                    state = dis.readBoolean();
                    table.setCell(i, j, state);
                    table.getCell(i, j).setOldState(state);
                }
            }
            dis.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
