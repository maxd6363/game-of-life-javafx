package app.model.serializationTable.tableLoader;

import app.model.table.Table;
import java.io.File;

/**
 * interface gèrant le chargement d'une table
 */
public interface TableLoader {
    /**
     * charge une table à partir d'un fichier
     * @param file ficher ou se trouve la table
     * @param table table à remplir
     */
    void load(File file, Table table);
}
