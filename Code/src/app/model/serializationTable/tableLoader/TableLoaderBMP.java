package app.model.serializationTable.tableLoader;

import app.model.table.Table;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class TableLoaderBMP implements TableLoader {
    @Override
    public void load(File file, Table table) {
        try {
            BufferedImage img = ImageIO.read(file);

            int state;
            int rows = img.getData().getHeight();
            int cols = img.getData().getWidth();

            table.setRows(rows);
            table.setCols(cols);

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    state = img.getRGB(j,i);
                    if(state == -1){
                        table.setCell(i, j, false);
                        table.getCell(i, j).setOldState(false);
                    }
                    else{
                        table.setCell(i, j, true);
                        table.getCell(i, j).setOldState(true);
                    }

                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
