package app.model.serializationTable.tableSaver;

import app.model.table.Table;
import java.io.File;

/**
 * interface gèrant la sauvegarde d'une table
 */
public interface TableSaver {

    /**
     * sauvegarde une table dans un fichier
     * @param file ficher où enregistrer
     * @param table table à sauvegarder
     */
    boolean save(File file, Table table);

}
