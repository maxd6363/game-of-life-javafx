package app.model.serializationTable.tableSaver;

import app.model.table.Table;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * classe implémentant TableSaver, sauvegarde une table dans un fichier binaire
 */
public class TableSaverBin implements TableSaver {

    /**
     * sauvegarde une table dans un fichier
     * @param file ficher où enregistrer
     * @param table table à sauvegarder
     */
    @Override
    public boolean save(File file, Table table) {
        try {
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(file));
            dos.writeInt(table.getRows());
            dos.writeInt(table.getCols());
            for (int i = 0; i < table.getRows(); i++) {
                for (int j = 0; j < table.getCols(); j++) {
                    dos.writeBoolean(table.getCell(i, j).getState());
                }
            }
            dos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();

        }
        return false;
    }
}
