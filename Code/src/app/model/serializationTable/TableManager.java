package app.model.serializationTable;

import app.model.serializationTable.tableLoader.TableLoader;
import app.model.serializationTable.tableLoader.TableLoaderBMP;
import app.model.serializationTable.tableLoader.TableLoaderBin;
import app.model.serializationTable.tableLoader.TableLoaderRLE;
import app.model.serializationTable.tableSaver.TableSaver;
import app.model.table.Table;
import app.model.utilities.UnknownFileExtensionException;
import app.model.utilities.Utils;
import java.io.File;

/**
 * manager de sérialisation de table
 */
public class TableManager {

    private TableSaver tableSaver;
    private TableLoader tableLoader;

    /**
     * constructeur à injection de dépendance
     * @param tableSaver tableSaver stratégie
     * @param tableLoader tableLoader stratégie
     */
    public TableManager(TableSaver tableSaver, TableLoader tableLoader) {
        this.tableSaver = tableSaver;
        this.tableLoader = tableLoader;
    }


    /**
     * sauvegarde une table dans un fichier
     * @param file ficher où enregistrer
     * @param table table à sauvegarder
     */
    public void save(File file, Table table) {
        if (table == null || file == null) {
            throw new NullPointerException();
        }
        tableSaver.save(file, table);
    }

    /**
     * charge une table à partir d'un fichier
     * @param file ficher ou se trouve la table
     * @param table table à remplir
     */
    public void load(File file, Table table) {

        switch (Utils.getFileExtension(file)) {
            case ".rle":
                tableLoader = new TableLoaderRLE();
                break;
            case ".gol":
                tableLoader = new TableLoaderBin();
                break;
            case ".bmp":
                tableLoader = new TableLoaderBMP();
                break;
            default:
                throw new UnknownFileExtensionException("Unknown file extension", null);
        }
        tableLoader.load(file, table);
    }

}
