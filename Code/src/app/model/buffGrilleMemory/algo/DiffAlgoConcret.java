package app.model.buffGrilleMemory.algo;
import java.util.ArrayList;
import java.util.List;

/**
 * Permet de représenter l'algorithme de calcule des différences entre tableau de cellules. Va choisir quelles seront les données sauvegardées en mémoire.
 * @param List<int[]> : le type du tableau de cellules
 * @param boolean[][] : le type de la liste des changements
 */
public class DiffAlgoConcret implements Algo<List<int[]>,boolean[][]> {

    /**
     * Permet de calculer les différences entre l'ancien tableau de cells, et le nouveau.
     * @param prev : La première version du tableau
     * @param next : La version suivante
     * @return : une liste de différences
     */
    @Override
    public List<int[]> diffAlgo(boolean[][] prev, boolean[][] next) {
        if(next == null){
           return new ArrayList<>();
        }
        else if(prev == null){
            prev = new boolean[next.length][next[0].length];
        }
        List<int[]> res = new ArrayList<int[]>();
        int xSize = Math.min(prev.length,next.length);
        int ySize = Math.min(prev[0].length,next[0].length);
        for(int i = 0; i<xSize;i++){
            for(int j=0;j<ySize;j++){
                if((prev[i][j] && !next[i][j]) || (!prev[i][j] && next[i][j])){
                    int[] a = {i,j};
                    res.add(a);
                }
            }

        }
        return res;
    }

    /**
     * Permet d'appliquer une liste de changement à une grille de cellules.
     * @param origine : la grille de cellules de base
     * @param diff : la liste des changements
     * @return : une nouvelle Liste changée.
     */
    @Override
    public boolean[][] applyChanges(boolean[][] origine, List<int[]> diff){
        if(diff == null || diff.isEmpty()) return origine;
        boolean[][] res = origine;
        int i = 0;
        for( i=0;i<diff.size();i++){
            int a[] = diff.get(i);
            if(a[0] > origine.length || a[1] > origine[0].length) continue;
            res[a[0]][a[1]] = !res[a[0]][a[1]];
        }
        return res;
    }
}
