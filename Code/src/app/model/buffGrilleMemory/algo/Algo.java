package app.model.buffGrilleMemory.algo;

/**
 * Permet de représenter l'algorithme de calcule des différences entre tableau de cellules. Va choisir quelles seront les données sauvegardées en mémoire.
 * @param <T> : le type du tableau de cellules
 * @param <K> : le type de la liste des changements
 */
public interface Algo<T,K> {
    /**
     * Permet de calculer les différences entre l'ancien tableau de cells, et le nouveau.
     * @param prev : La première version du tableau
     * @param next : La version suivante
     * @return : une liste de différences
     */
    public T diffAlgo(K prev, K next);

    /**
     * Permet d'appliquer une liste de changement à une grille de cellules.
     * @param origine : la grille de cellules de base
     * @param diff : la liste des changements
     * @return : une nouvelle Liste changée.
     */
    public K applyChanges(K origine, T diff);
}
