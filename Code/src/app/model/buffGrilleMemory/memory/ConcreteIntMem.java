package app.model.buffGrilleMemory.memory;
import java.util.LinkedList;
import java.util.List;

/**
 * ConcreteIntMem permet de gérer une mémoire de type buffer qui stock les différences de tableaux boolean sous la forme de ints.
 */
public class ConcreteIntMem extends IntMem {
    private int index;

    /**
     * Constructeur de ConcreteIntMem
     * @param BUFFSIZE : la taille maximale du buffer
     */
    public ConcreteIntMem(int BUFFSIZE){
        super(new LinkedList<List<int[]>>(),BUFFSIZE);
        index = 0;
    }

    /**
     * Permet d'ajouter un element de type T à la mémoire
     * @param elem : La liste des différences.
     */
    @Override
    public void add(List<int[]> elem) {
        System.out.println("adding ! before : " + size());
        if(index == size()-1){
            if(this.size() == this.getBUFFSIZE()){
                this.pop();
            }
        }
        getMem().add(elem);
        index = size()-1;
        System.out.println("AFTER ADDING SIZE MEM : " + size());
    }

    /**
     * Permet de supprimer le dernier élément de la mémoire
     * @return : le tableau de différences
     */
    @Override
    public List<int[]> removeLast() {
        return this.getMem().remove(this.size()-1);
    }

    /**
     * Permet de supprimer le premier élément de la mémoire
     * @return : le tableau de cells
     */
    @Override
    public List<int[]> pop() {
        return this.getMem().remove(0);
    }

    /**
     * Permet de connaitre la taille de la mémoire
     * @return : la taille sous la forme d'un entier
     */
    @Override
    public int size() {
        return this.getMem().size();
    }

    /**
     * Permet de connaitre l'element à l'index index
     * @param index : un entier qui décrit l'index à rechercher
     * @return : le tableau de différences
     */
    @Override
    public List<int[]> getIndex(int index) {
        return this.getMem().get(index);
    }

    /**
     * Permet de récupérer l'élément précédent dans la mémoire
     * @return : le tableau de différences
     */
    @Override
    public List<int[]> getPrev() {
        if(size() == 0) return null;
        if(index == 0) return null;
        //index--;
        List<int[]> tmp = getMem().get(index--);
        System.out.println("## PREV : " + index);
        return tmp;

    }

    /**
     * Permet de récupérer l'élement suivant dans la mémoire
     * @return : le tableau de différences
     */
    @Override
    public List<int[]> getNext() {
        if(size() == 0) return null;
        if(index == size()-1) return null;
        List<int[]> tmp = getMem().get(++index);
        System.out.println("## NEXT : " + index);
        //index++;
        return tmp;
    }

    /**
     * Permet de mettre à jour l'élément elem dans la mémoire
     * @param elem : le tableau de cells à mettre à jour
     */
    @Override
    public void update(List<int[]> elem) {
        //not implemented
    }

    /**
     * Permet de récupérer l'élément courant dans la mémoire
     * @return : le tableau de cells
     */
    @Override
    public List<int[]> getCurrent() {
        return getMem().get(index);
    }

    /**
     * Permet de vider la mémoire
     */
    @Override
    public void flush() {
        getMem().clear();
    }


}
