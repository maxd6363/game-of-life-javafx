package app.model.buffGrilleMemory.memory;
import java.util.List;

public abstract class IntMem implements Mem<List<int[]>> {
    /**
     * Contient la liste des listes de changements
     */
    private List<List<int[]>> mem;
    /**
     * Définie la taille maximale de la mémoire
     */
    private int BUFFSIZE = 0;

    public IntMem(List<List<int[]>> list, int buffsize){
        this.mem = list;
        this.BUFFSIZE = buffsize;
    }

    protected List<List<int[]>> getMem(){return mem;}
    protected void setMem(List<List<int[]>> list){this.mem = list;}
    protected int getBUFFSIZE(){return BUFFSIZE;}
}
