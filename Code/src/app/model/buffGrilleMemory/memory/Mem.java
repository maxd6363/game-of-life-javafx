package app.model.buffGrilleMemory.memory;

/**
 * Permet de représenter la mémoire que doit disposer un buffer
 * @param <T> : Le type de la mémoire
 */
public interface Mem<T> {

    /**
     * Permet d'ajouter un element de type T à la mémoire
     * @param elem : La liste des différences.
     */
    void add(T elem);

    /**
     * Permet de supprimer le dernier élément de la mémoire
     * @return : le tableau de différences
     */
    T removeLast();

    /**
     * Permet de supprimer le premier élément de la mémoire
     * @return : le tableau de cells
     */
    T pop();

    /**
     * Permet de connaitre la taille de la mémoire
     * @return : la taille sous la forme d'un entier
     */
    int size();

    /**
     * Permet de connaitre l'element à l'index index
     * @param index : un entier qui décrit l'index à rechercher
     * @return : le tableau de différences
     */
    T getIndex(int index);

    /**
     * Permet de récupérer l'élément précédent dans la mémoire
     * @return : le tableau de différences
     */
    T getPrev();

    /**
     * Permet de récupérer l'élement suivant dans la mémoire
     * @return : le tableau de différences
     */
    T getNext();

    /**
     * Permet de mettre à jour l'élément elem dans la mémoire
     * @param elem : le tableau de cells à mettre à jour
     */
    void update(T elem);

    /**
     * Permet de récupérer l'élément courant dans la mémoire
     * @return : le tableau de cells
     */
    T getCurrent();

    /**
     * Permet de vider la mémoire
     */
    void flush();
}
