package app.model.buffGrilleMemory.buffer;

import app.model.buffGrilleMemory.algo.Algo;
import app.model.buffGrilleMemory.memory.IntMem;

import java.util.List;

public abstract class IntBoolBuff implements Buff<List<int[]>,boolean[][]> {
    private Algo<List<int[]>,boolean[][]> algorithm;
    private IntMem memory;

    /**
     * Constructeur de IntBoolBuff
     * @param algo : permet de définir l'algorithme de calcule à utiliser
     * @param mem : permet de définir le type de mémoire à utiliser, ici une mémoire de type IntMem
     */
    public IntBoolBuff(Algo<List<int[]>,boolean[][]> algo, IntMem mem){
        this.algorithm = algo;
        this.memory = mem;
    }

    /**
     * Permet de récupérer l'algorithme
     * @return : L'algorithme
     */
    protected Algo<List<int[]>,boolean[][]> getAlgorithm(){return algorithm;}

    /**
     * Permet de définir l'algorithme à utiliser
     * @param algo : L'algorithme avec lequel on va remplacer l'ancien
     */
    protected void setAlgorithm(Algo<List<int[]>,boolean[][]> algo){this.algorithm = algo;}

    /**
     * Permet de récupérer la mémoire
     * @return : La mémoire
     */
    protected IntMem getMemory(){return memory;}

    /**
     * Permet de redéfinir la mémoire
     * @param value : La mémoire avec laquelle on va remplacer l'ancienne
     */
    protected void setMemory(IntMem value){this.memory = value;}

}
