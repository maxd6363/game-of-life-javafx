package app.model.buffGrilleMemory.buffer;
import app.model.buffGrilleMemory.algo.DiffAlgoConcret;
import app.model.buffGrilleMemory.memory.ConcreteIntMem;
import java.util.List;

public class ConcreteBuffer extends IntBoolBuff {
    private boolean[][] currentCells = null;
    public ConcreteBuffer(int BUFFER_SIZE) {
        super(new DiffAlgoConcret(), new ConcreteIntMem(BUFFER_SIZE));
    }

    /**
     * Permet de set le tableau de cells actuel.
     * @param cells le tableau de cells
     */
    public void setCurrentCells(boolean[][] cells){this.currentCells = cells;}

    /**
     * Permet de savoir si la mémoire est vide ou non.
     * @return : True si vrai, false sinon
     */
    @Override
    public boolean isEmpty() {
        return (getMemory().size() == 0) ? true : false;
    }

    /**
     * Permet de supprimer toute la mémoire.
     */
    @Override
    public void flushBuffer() {
        getMemory().flush();
    }

    /**
     * Permet d'ajouter un tableau de cells en mémoire, dans la mémoire spécifique
     * @param cells : le tableau de cells
     */
    @Override
    public void addToMem(boolean[][] cells) {
        if(currentCells == null){
            currentCells = new boolean[cells.length][cells[0].length];
        }
        getMemory().add(getAlgorithm().diffAlgo(currentCells,cells));
        currentCells = cells;
    }

    /**
     * Permet de récupérer le tableau de cells précédents
     * @return : le tableau de cells
     */
    @Override
    public boolean[][] getPrevious() {
        System.out.println("previous");
        List<int[]> tmp = getMemory().getPrev();
        if(tmp == null) return null;
        boolean[][] tab = getAlgorithm().applyChanges(currentCells,tmp);
        currentCells = tab;
        return tab;
    }

    /**
     * Permet de récupérer le tableau de cells suivant
     * @return : le tableau de cells
     */
    @Override
    public boolean[][] getNext() {
        System.out.println("next");
        List<int[]> tmp = getMemory().getNext();
        if(tmp == null) return null;
        boolean[][] tab = getAlgorithm().applyChanges(currentCells,tmp);
        currentCells = tab;
        return tab;
    }
}
