package app.model.buffGrilleMemory.buffer;

/**
 * Permet de représenter le type de buffer à utiliser
 * @param <T>
 * @param <K>
 */
public interface Buff<T,K> {
    /**
     * Permet d'ajouter un tableau de cells en mémoire
     * @param cells : le tableau de cells
     */
    public void addToMem(K cells);

    /**
     * Permet de récupérer le tableau de cells précédents
     * @return : le tableau de cells
     */
    public K getPrevious();

    /**
     * Permet de récupérer le tableau de cells suivant
     * @return : le tableau de cells
     */
    public K getNext();

    /**
     * Permet de set le tableau de cells actuel.
     * @param cells le tableau de cells
     */
    public void setCurrentCells(K cells);

    /**
     * Permet de savoir si la mémoire est vide ou non.
     * @return : True si vrai, false sinon
     */
    public boolean isEmpty();

    /**
     * Permet de supprimer toute la mémoire.
     */
    public void flushBuffer();
}
