package app.model.utilities;

/**
 * custom exception pour les extensions de fichiers
 */
public class UnknownFileExtensionException extends RuntimeException {

    /**
     * constructeur
     * @param message message
     * @param error erreur à envoyer
     */
    public UnknownFileExtensionException(String message, Throwable error) {
        super(message, error);
    }


}
