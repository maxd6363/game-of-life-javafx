package app.model.utilities;

import java.io.File;
import java.util.Random;

/**
 * classe static contenant les méthodes utils au projet
 */
public class Utils {

    private static Random random;

    static {
        random = new Random();
    }


    /**
     * analyse le fichier pour avoir son extension
     * @param file fichier à analyser
     * @return extension
     */
    public static String getFileExtension(File file) {
        String extension = "";

        try {
            if (file != null && file.exists()) {
                String name = file.getName();
                extension = name.substring(name.lastIndexOf("."));
            }
        } catch (Exception e) {
            extension = "";
        }

        return extension;
    }


    /**
     * récupère le random
     * @return
     */
    public static Random getRandom() {
        return random;
    }

    /**
     * snap une valeur au pixel prêt pour éviter le flou sur le canvas
     * @param y valeur
     * @return valeur snappée
     */
    public static double snap(double y) {
        return ((int) y) + .5;
    }


}
