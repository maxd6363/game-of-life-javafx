package app.model;

import app.model.rules.Rule;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * classe gérant le registre de règles
 */
public class PropertiesRegistre {
    private static final String FOLDER_NAME = ".GameOfLifeConfig";
    private static final String REGISTRE_NAME = "registreRules.config";
    private String homeName = System.getProperty("user.home");
    private File registre;
    private File folder;

    /**
     * constructeur
     */
    public PropertiesRegistre() {
        this.folder = new File(new StringBuilder(homeName).append("/").append(FOLDER_NAME).toString());
        this.registre = new File(new StringBuilder(homeName).append("/").append(FOLDER_NAME).append("/").append(REGISTRE_NAME).toString());
    }

    /**
     * ajoute un path de règle au registre
     *
     * @param r règle à sauver
     */
    public void saveOneRuleToRegistre(Rule r) {
        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(registre, true)
            );
            writer.newLine();
            writer.write(r.getRulePath());
            writer.close();
        } catch (Exception e) {

        }
    }


    /**
     * ajoute des path de règles au registre
     *
     * @param rules règles à sauver
     */
    public void saveRulesToRegistre(Collection<Rule> rules) {
        if (!folder.exists()) {
            folder.mkdirs();
        }
        try {
            FileWriter fileWriter = new FileWriter(registre);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            for (Rule r : rules) {
                if (r.getRulePath() != null && !r.getRulePath().isEmpty()) printWriter.println(r.getRulePath());
            }
            printWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * ajoute des path au registre
     *
     * @param paths les paths
     */
    public void saveAllStringToRegistre(Collection<String> paths) {
        if (!folder.exists()) {
            folder.mkdirs();
        }
        try {
            FileWriter fileWriter = new FileWriter(registre);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            for (String s : paths) {
                if (s != null && !s.isEmpty()) printWriter.println(s);
            }
            printWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * récupère les paths dans le registre
     *
     * @return les paths dans le registre
     */
    public Collection<String> getRulePathsInRegister() {
        List<String> result = null;
        if (!folder.exists()) {
            folder.mkdirs();
        }
        if (!registre.exists()) {
            return null;
        }
        try {
            result = Files.readAllLines(Paths.get(registre.getPath()));
            for (String s : result) {
                System.out.println("[INFO] rules dans registre : " + s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Regarde si les fichiers pointé par le registre existe encore, sinon il les supprime
     * Pas implémenté
     */
    public void actualizeRegister() {

        Collection<String> paths = getRulePathsInRegister();
        Collection<String> pathsRes = new ArrayList<>();
        for(String s : paths) {
            if(new File(s).exists()){ //Le new est il optimisé ? ...
                pathsRes.add(s);
            }else{
                System.out.println("[INFO] : Optimisation, suppression d'un chemin inexistant dans le registre des Rules");
            }
        }
        saveAllStringToRegistre(pathsRes);

    }
}
