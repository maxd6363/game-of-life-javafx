package app.model.serializationRule.ruleLoader;


import app.model.rules.Rule;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.*;

/**
 * classe implémentant RuleLoader, charge les règles à partir d'un fichier binaire
 */
public class RuleLoaderBin implements RuleLoader {

    /**
     * charge une règles à partir d'un path
     * @param path path
     * @return règle
     */
    public Rule load(String path) {
        Rule tmp;
        try {
            if (!new File(path).exists()) return null;
            FileInputStream fos = new FileInputStream(path);
            ObjectInputStream oos = new ObjectInputStream(fos);
            tmp = (Rule) oos.readObject();
            oos.close();
            return tmp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * charge toutes les règles d'une collection de path
     * @param pathList collection de path
     * @return TreeMap de Integer et Rule
     */
    public Collection<Rule> loadAll(Collection<String> pathList) {
        Collection<Rule> result = new HashSet<Rule>();
        if (pathList == null) return result;
        for (String s : pathList) {
            Rule r = load(s);
            if (r != null) result.add(r);
        }
        return result;
    }
}