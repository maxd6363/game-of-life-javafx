package app.model.serializationRule.ruleLoader;

import app.model.rules.Rule;
import javafx.scene.paint.Color;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

/**
 * interface gèrant le chargement des règles
 */
public interface RuleLoader {

    /**
     * charge une règles à partir d'un path
     * @param path path
     * @return règle
     */
    Rule load(String path);

    /**
     * charge toutes les règles d'une collection de path
     * @param pathList collection de path
     * @return TreeMap de Integer et Rule
     */
    Collection<Rule> loadAll(Collection<String> pathList);
}
