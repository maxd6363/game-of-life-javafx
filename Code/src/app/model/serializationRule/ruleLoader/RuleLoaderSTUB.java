package app.model.serializationRule.ruleLoader;

import app.model.rules.Rule;

import java.util.*;

public class RuleLoaderSTUB implements RuleLoader {

    /**
     * ne charge rien
     * @param nom nom
     * @return null
     */
    @Override
    public Rule load(String nom) {
        return null;
    }


    /**
     * charge toutes les règles pour le test
     * @param pathList collection de path
     * @return TreeMap de Integer et Rule
     */
    @Override
    public Collection<Rule> loadAll(Collection<String> pathList) {
        Collection<Rule> rules = new HashSet<Rule>();
        rules.add(new Rule(0, "Rule 1", new TreeSet<Integer>(List.of(2, 3)), new TreeSet<Integer>(List.of(4, 3))));
        rules.add(new Rule(1, "Rule 2", new TreeSet<Integer>(List.of(4, 7)), new TreeSet<Integer>(List.of(8, 0))));
        rules.add(new Rule(2, "Rule 3", new TreeSet<Integer>(List.of(1, 8)), new TreeSet<Integer>(List.of(5, 2))));
        rules.add(new Rule(3, "Rule 4", new TreeSet<Integer>(List.of(2, 0)), new TreeSet<Integer>(List.of(1, 3))));
        return rules;
    }

}
