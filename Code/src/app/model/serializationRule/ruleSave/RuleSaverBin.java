package app.model.serializationRule.ruleSave;

import app.model.rules.Rule;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * classe implémentant RuleSaver, sauvegarde les règles dans un fichier binaire
 */
public class RuleSaverBin implements RuleSaver {


    /**
     * sauvegarde une règle
     * @param r règle
     * @return résultat de la sauvegarde
     */
    @Override
    public boolean save(Rule r){
        try {

            File test = new File(r.getRulePath());
            FileOutputStream fos = new FileOutputStream(test);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(r);
            oos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}
