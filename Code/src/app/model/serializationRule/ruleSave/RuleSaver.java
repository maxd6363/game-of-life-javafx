package app.model.serializationRule.ruleSave;

import app.model.rules.Rule;

/**
 * interface gèrant la sauvegarde des règles
 */
public interface RuleSaver {
    /**
     * sauvegarde une règle
     * @param r règle
     * @return résultat de la sauvegarde
     */
    boolean save(Rule r);
}
