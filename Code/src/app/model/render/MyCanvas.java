package app.model.render;

import app.model.render.Functionalities.Functionality;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import java.util.ArrayList;
import java.util.Collection;

/**
 * classe étendue de javafx.scene.canvas.Canvas pour le faire adaptatif à la fenêtre
 */
public abstract class MyCanvas extends Canvas {

    private final static Color DEFAULT_ALIVE_COLOR =  Color.BLACK;
    private final static Color DEFAULT_DEAD_COLOR =  Color.WHITE;
    private final static Color DEFAULT_GRID_COLOR =  Color.BLACK;

    private Collection<Functionality> functionalities = new ArrayList<>();
    private GraphicsContext graphicsContext = getGraphicsContext2D();
    private ObjectProperty<Color> aliveColor;
    private ObjectProperty<Color> deadColor;
    private ObjectProperty<Color> gridColor;




    /**
     * constructeur
     */
    public MyCanvas() {
        super();
        aliveColor = new SimpleObjectProperty<>(DEFAULT_ALIVE_COLOR);
        deadColor = new SimpleObjectProperty<>(DEFAULT_DEAD_COLOR);
        gridColor = new SimpleObjectProperty<>(DEFAULT_GRID_COLOR);
    }

    public Collection<Functionality> getAllFunctionnalites(){
        return functionalities;
    }

    public ObjectProperty<Color> aliveColorProperty(){
        return aliveColor;
    }
    public ObjectProperty<Color> deadColorProperty(){
        return deadColor;
    }
    public ObjectProperty<Color> gridColorProperty(){
        return gridColor;
    }

    public Color getAliveColor() {
        return aliveColor.getValue();
    }

    public void setAliveColor(Color c) {
        this.aliveColor.setValue(c);
    }

    public Color getDeadColor() {
        return deadColor.getValue();
    }

    public void setDeadColor(Color c) {
        this.deadColor.setValue(c);
    }

    public Color getGridColor() {
        return gridColor.getValue();
    }

    public void setGridColor(Color c) {
        this.gridColor.setValue(c);
    }

    /**
     * effectue le render de chaque fonctionnalités
     */
    public void render() {
        for (Functionality function : functionalities) {
            function.render();
        }
    }

    /**
     * renvoie une fonctionnalité à partir d'un nom
     * @param name nom de la fonctionnalité
     * @return la fonctionnalité correspondante
     */
    public Functionality getFunctionality(String name) {
        for (Functionality functionality : functionalities) {
            if (functionality.getName().equals(name)) {
                return functionality;
            }
        }
        System.out.println("[INFO] : LA FUNCTIONALITY n'existe pas ! : " + name);
        return null;
    }

    /**
     * ajoute une fonctionnalité
     * @param functionality fonctionnalité à ajouter
     * @return résultat de l'ajout
     */
    public boolean addFunctionnality(Functionality functionality) {
        functionality.setGraphicsContext(graphicsContext);
        return functionalities.add(functionality);
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double maxHeight(double width) {
        return Double.POSITIVE_INFINITY;
    }

    @Override
    public double maxWidth(double height) {
        return Double.POSITIVE_INFINITY;
    }

    @Override
    public double minWidth(double height) {
        return 1D;
    }

    @Override
    public double minHeight(double width) {
        return 1D;
    }

    @Override
    public void resize(double width, double height) {
        this.setWidth(width);
        this.setHeight(height);
    }
}

