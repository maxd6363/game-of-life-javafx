package app.model.render;

import app.model.buffGrilleMemory.buffer.Buff;
import app.model.buffGrilleMemory.buffer.ConcreteBuffer;
import app.model.render.Functionalities.ColoredFunction;
import app.model.render.Functionalities.SquareCellFunction;
import app.model.render.Functionalities.SquareGridFunction;
import app.model.table.Table;
import javafx.animation.AnimationTimer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;

import java.util.List;

/**
 * classe gérant le rendu global sur le canvas, étend javafx.animation.AnimationTime
 */
public class Renderer extends AnimationTimer {

    private static final int BUFFER_SIZE = 50;
    private static final int DEFAULT_FPS = 60;
    private static final String SQUARECELLFUNCTION = "SquareCellFunction";
    private static final String SQUAREGRIDFUNCTION = "SquareGridFunction";
    private static final String COLOREDFUNCTION = "ColoredFunction";

    private Table table;
    private MyCanvas myCanvas;
    private DoubleProperty frameRate;
    private long oldFrame;
    private long elapsedNanoSeconds = 0;

    private Buff<List<int[]>,boolean[][]> BufferVersion2;

    private BooleanProperty runningState;
    private SquareCellFunction squareCellFunction;
    private SquareGridFunction squareGridFunction;

    private BooleanProperty rgbMode;


    /**
     * constructeur
     * @param table table à rendre
     */
    public Renderer(Table table) {
        runningState = new SimpleBooleanProperty();
        rgbMode = new SimpleBooleanProperty();
        frameRate = new SimpleDoubleProperty(DEFAULT_FPS);
        setRunningState(false);
        oldFrame = System.nanoTime();
        this.table = table;
        myCanvas = new SimpleCanvas();
        squareCellFunction = new SquareCellFunction(myCanvas, table, table.getRows(), table.getCols(), SQUARECELLFUNCTION);
        squareGridFunction = new SquareGridFunction(myCanvas, table.getRows(), table.getCols(), SQUAREGRIDFUNCTION);

        Bindings.bindBidirectional(squareCellFunction.colsProperty(),table.colsProperty());
        Bindings.bindBidirectional(squareCellFunction.rowsProperty(),table.rowsProperty());
        Bindings.bindBidirectional(squareGridFunction.colsProperty(),table.colsProperty());
        Bindings.bindBidirectional(squareGridFunction.rowsProperty(),table.rowsProperty());


        myCanvas.addFunctionnality(new ColoredFunction(myCanvas, COLOREDFUNCTION));
        myCanvas.addFunctionnality(squareCellFunction);
        myCanvas.addFunctionnality(squareGridFunction);

        BufferVersion2 = new ConcreteBuffer(BUFFER_SIZE);

    }

    /**
     * =========================== Getter Setter AREA ================================
     */
    public DoubleProperty frameRateProperty(){
        return frameRate;
    }
    public double getFrameRate() {
        return frameRate.getValue();
    }
    public void setFrameRate(double frameRate) {
        this.frameRate.setValue(frameRate);
    }
    public BooleanProperty runningStateProperty() {
        return runningState;
    }
    public boolean getRunningState() {
        return runningState.get();
    }
    private void setRunningState(boolean value) {
        runningState.set(value);
    }
    public BooleanProperty rgbModeProperty() {
        return rgbMode;
    }
    public boolean getRgbMode() {
        return rgbMode.get();
    }
    private void setRgbMode(boolean value) {
        rgbMode.set(value);
    }
    public MyCanvas getMyCanvas() {
        return myCanvas;
    }
    /**
     * =========================== ================== ================================
     */

    /**
     * change l'état du renderer
     */
    public void changeRgbMode() {
        setRgbMode(!getRgbMode());
    }


    /**
     * change l'état du renderer
     */
    public void changeStateRunning() {
        setRunningState(!getRunningState());
    }

    /**
     * met en marche le renderer
     */
    public void pauseRender() {
        setRunningState(false);
    }

    /**
     * met en pause le renderer
     */
    public void stopRunning() {
        setRunningState(false);
    }

    /**
     * calcule la prochaine étape
     */
    public void nextFrame() {
        if (getRunningState()) {
            pauseRender();
        }
        internalNextFrame();
    }

    /**
     * Permet de vider la zone de dessin
     */
    public void clearArea(){
        table.killAllCells();
        BufferVersion2.flushBuffer();
    }

    private void internalNextFrame(){
        boolean[][] tmp = BufferVersion2.getNext();
        if(tmp == null){
            if(BufferVersion2.isEmpty()){
                System.out.println("first use !");
                BufferVersion2.addToMem(table.getCells());
            }
            table.processOneGeneration();
            BufferVersion2.addToMem(table.getCells());

        }
        else{
            table.setCells(tmp);
        }
    }


    /**
     * restaure la dernière frame
     */
    public void previousFrame() {
        if (getRunningState()) {
            pauseRender();
        }
        boolean[][] tmp = BufferVersion2.getPrevious();
        if (tmp != null) {
            table.setCells(tmp);
        } else {
        }
        if (table.getIteration() > 0)
            table.setIteration(table.getIteration() - 1);

    }

    public void render() {
        start();
    }

    /**
     * méthode gérant le rendu global en fonction du temps pour avoir un frame rate constant
     * @param l : temps systeme
     */
    @Override
    public void handle(long l) {
        elapsedNanoSeconds += (l - oldFrame); //On accumule le temps dans cette variable
        oldFrame = l;
        long sleepTime = (long) ((1.0 / getFrameRate()) * 1000000000);
        if (elapsedNanoSeconds > sleepTime) { //Lorsque le temps depasse la limite, alors on va render
            elapsedNanoSeconds = 0;
            if (getRunningState()) {
                internalNextFrame();
            }
        }
        //si on rend le canvas à chaque itération de l'animation canvas, la souris ne lag pas
        if(getRgbMode()){
            myCanvas.aliveColorProperty().setValue(Color.hsb( (myCanvas.aliveColorProperty().getValue().getHue()+1 )%360,1,1));
        }
        myCanvas.render();
    }



}
