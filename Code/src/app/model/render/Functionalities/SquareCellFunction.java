package app.model.render.Functionalities;

import app.model.render.MyCanvas;
import app.model.table.Table;
import app.model.utilities.Utils;
import javafx.beans.property.Property;
import javafx.beans.property.ReadOnlyObjectWrapper;

/**
 * classe gérant les cellules carrées sur le canvas
 */
public class SquareCellFunction extends Functionality {

    protected Property<Integer> rows;
    protected Property<Integer> cols;


    private Table table;

    /**
     * constructeur
     *
     * @param myCanvas   le canvas où rendre les cellules
     * @param table      la table où prendre les cellules
     * @param numberRows nombre de lignes
     * @param numberCols nombre de colonnes
     * @param name       nom de la fonctionnalité
     */
    public SquareCellFunction(MyCanvas myCanvas, Table table, int numberRows, int numberCols, String name) {
        super(myCanvas);
        super.setName(name);
        if (numberRows <= 0) numberRows = 1;
        if (numberCols <= 0) numberCols = 1;

        this.rows = new ReadOnlyObjectWrapper<>(this,"rows",numberRows);
        this.cols = new ReadOnlyObjectWrapper<>(this,"cols",numberRows);


        this.table = table;
    }

    public Property<Integer> rowsProperty() {
        return rows;
    }

    public int getRows() {
        return rows.getValue();
    }

    public void setRows(int value) {
        rows.setValue(value);
    }


    public Property<Integer> colsProperty() {
        return cols;
    }

    public int getCols() {
        return cols.getValue();
    }

    public void setCols(int value) {
        cols.setValue(value);
    }



    /**
     * gère le rendu les cellules sur le canvas
     */
    @Override
    public void render() {
        double cellWidth = myCanvas.getWidth() / getCols();
        double cellHeight = myCanvas.getHeight() / getRows();

        for (int i = 0; i < table.getRows(); i++) {
            for (int j = 0; j < table.getCols(); j++) {
                if (table.getCell(i, j).getState()) {
                    graphicsContext.fillRect(Utils.snap(j * cellWidth), Utils.snap(i * cellHeight), Utils.snap(cellWidth), Utils.snap(cellHeight));
                }
            }
        }
    }
}
