package app.model.render.Functionalities;

import app.model.render.MyCanvas;
import app.model.utilities.Utils;
import javafx.beans.property.Property;
import javafx.beans.property.ReadOnlyObjectWrapper;

/**
 * classe gérant la grille sur le canvas
 */
public class SquareGridFunction extends Functionality {

    protected Property<Integer> rows;
    protected Property<Integer> cols;


    /**
     * constructeur
     * @param myCanvas le canvas où rendre la grille
     * @param numberRows nombre de lignes
     * @param numberCols nombre de colonnes
     * @param name nom de la fonctionnalité
     */
    public SquareGridFunction(MyCanvas myCanvas, int numberRows, int numberCols, String name) {
        super(myCanvas);
        super.setName(name);
        if (numberRows <= 0) numberRows = 1;
        if (numberCols <= 0) numberCols = 1;
        this.rows = new ReadOnlyObjectWrapper<>(this,"rows",numberRows);
        this.cols = new ReadOnlyObjectWrapper<>(this,"cols",numberCols);

    }


    public Property<Integer> rowsProperty() {
        return rows;
    }

    public int getRows() {
        return rows.getValue();
    }

    public void setRows(int value) {
        rows.setValue(value);
    }


    public Property<Integer> colsProperty() {
        return cols;
    }

    public int getCols() {
        return cols.getValue();
    }

    public void setCols(int value) {
        cols.setValue(value);
    }


    /**
     * gère le rendu de la grille sur le canvas
     */
    @Override
    public void render() {
        double rowsSpacing = myCanvas.getHeight() / getRows();
        double colsSpacing = myCanvas.getWidth() / getCols();


        graphicsContext.setStroke(myCanvas.getGridColor());
        graphicsContext.setLineWidth(0.2);


        for (int i = 1; i < getRows(); i++) {
            graphicsContext.strokeLine(0, Utils.snap(i * rowsSpacing), Utils.snap(myCanvas.getWidth()), Utils.snap(i * rowsSpacing));

        }
        for (int i = 1; i < getCols(); i++) {
            graphicsContext.strokeLine(Utils.snap(i * colsSpacing), 0, Utils.snap(i * colsSpacing), Utils.snap(myCanvas.getHeight()));
        }


    }

}
