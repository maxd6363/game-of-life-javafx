package app.model.render.Functionalities;

import app.model.render.MyCanvas;
import javafx.scene.canvas.GraphicsContext;

/**
 * classe abstraite représentant les fonctionnalités
 */
public abstract class Functionality {
    protected MyCanvas myCanvas;
    protected GraphicsContext graphicsContext;
    private String name;

    /**
     * constructeur
     *
     * @param myCanvas le canvas où rendre les cellules
     */
    protected Functionality(MyCanvas myCanvas) {
        this.myCanvas = myCanvas;
        this.name = getClass().getSimpleName();
    }

    public String getName() {
        return this.name;
    }

    protected void setName(String value) {
        this.name = value;
    }


    /**
     * gère le rendu sur le canvas
     */
    public abstract void render();

    public void setGraphicsContext(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }


}
