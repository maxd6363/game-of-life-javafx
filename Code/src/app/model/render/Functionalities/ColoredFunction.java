package app.model.render.Functionalities;

import app.model.render.MyCanvas;
import javafx.scene.paint.Color;

/**
 * classe gérant les couleurs des cellules à l'écran
 */
public class ColoredFunction extends Functionality {

    /**
     * constructeur
     * @param myCanvas le canvas où rendre les cellules
     * @param name nom de la fonctionnalité
     */
    public ColoredFunction(MyCanvas myCanvas, String name) {
        super(myCanvas);
        super.setName(name);
    }

    /**
     * gère les couleurs du canvas
     */
    @Override
    public void render() {
        myCanvas.getGraphicsContext2D().setFill(myCanvas.getDeadColor());
        myCanvas.getGraphicsContext2D().fillRect(0, 0, myCanvas.getWidth(), myCanvas.getHeight());
        myCanvas.getGraphicsContext2D().setFill(myCanvas.getAliveColor());
    }


}
