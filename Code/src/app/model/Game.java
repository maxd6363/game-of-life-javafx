package app.model;

import app.model.render.Renderer;
import app.model.rules.RulesController;
import app.model.serializationTable.TableManager;
import app.model.serializationTable.tableLoader.TableLoaderBin;
import app.model.serializationTable.tableSaver.TableSaverBin;
import app.model.table.OrthogonalTable;
import app.model.table.Table;

/**
 * class représentant le jeu
 */
public class Game {

    private final static int DEFAULT_SIZE_ROWS = 100;
    private final static int DEFAULT_SIZE_COLS = 130;

    private Table table;
    private TableManager tableManager;
    private RulesController rulesController;
    private Renderer renderer;

    /**
     * constructeur
     * @param rulesController controleur de rules
     */
    public Game(RulesController rulesController) {
        this.rulesController = rulesController;

        table = new OrthogonalTable(rulesController, DEFAULT_SIZE_ROWS, DEFAULT_SIZE_COLS);
        tableManager = new TableManager(new TableSaverBin(), new TableLoaderBin());
        renderer = new Renderer(table);

    }

    /**
     * demarre la partie
     */
    public void launch() {
        renderer.render();
    }

    public Renderer getRenderer() {
        return renderer;
    }

    public Table getTable() {
        return table;
    }

    private void setTable(Table table) {
        if (table != null) {
            this.table = table;
            System.out.println(table);
        }
    }

    public RulesController getRulesController() {
        return rulesController;
    }

    public TableManager getTableManager() {
        return tableManager;
    }
}
