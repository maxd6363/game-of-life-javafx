# Game of Life JavaFX

## Auteurs

**Hector Piteau** et **Maxime Poulain** - Groupe 3 - Deuxième année

Université Clermont Auvergne - IUT Informatique - Clermont Ferrand




## Contexte 

Le jeu de la vie est un automate cellulaire crée par Mr Conway dans les années 70, c'est le plus connu de tous les automates cellulaires, de plus il est Turing-Complet.
Tout l'intérêt de ce jeu est que ce sont les mathématiques qui font la partie, le joueur ne fait qu'observer le déroulement de la simulation. Le jeu est composé d'une grille de cellules pouvant être dans deux états différents : vivante (en noir) ou morte (en blanc).
Chaque cellule a 8 voisins, les différents scénarios définissent les conditions où une cellule prend vie ou meurt, dans le jeu originel les conditions sont : une cellule morte possédant exactement trois voisines vivantes prend vie et une cellule vivante possédant deux ou trois voisines vivantes reste en vie, sinon elle meurt. Cependant on peut imaginer enormement de scénarios, 2^9 * 2^9 = 512^2 = 262144 possibilités.

## Règles du jeu classique

* 2 x 2 : (chaotique)
        - Survie : 1,2,5
        - Naitre : 3,6
* 34 Life : (exponentiel)
        - Survie : 3,4
        - Naitre : 3,4
* Amoeba : (chaotique)
        - Survie : 1,3,5,8
        - Naitre : 3,5,7
* Assimilation : (stable)
        - Survie : 4,5,6,7
        - Naitre : 3,4,5
* Coagulation : (exponentiel)
        - Survie : 2,3,,6,7,8
        - Naitre : 3,7,8
* Conway's Life : (chaotique)
        - Survie : 2,3
        - Naitre : 3
* Coral : (exponentiel)
        - Survie : 4,5,6,7,8
        - Naitre : 3
* Day & Night : (stable)
        - Survie : 3,4,6,7,8
        - Naitre : 3,6,7,8
* Diamoeba : (chaotique)
        - Survie : 5,6,7,8
        - Naitre : 3,5,7,8
* Flakes : (expansion)
        - Survie : 0,1,2,3,4,5,6,7,8
        - Naitre : 3
* Gnarl : (exponentiel)
        - Survie : 1
        - Naitre : 1
* High Life : (chaotique) 
        - Survie : 2,3
        - Naitre : 3,6
* Inverse Life : (chaotique) 
        - Survie : 3,4,6,7,8
        - Naitre : 0,1,2,3,4,7,8
* Long Life : (stable) 
        - Survie : 5
        - Naitre : 3,4,5
* Maze : (exponentiel)
        - Survie : 1,2,3,4,5
        - Naitre : 3
* Mazectric : (exponentiel)
        - Survie : 1,2,3,4
        - Naitre : 3
* Move : (stable)
        - Survie : 2,4,5
        - Naitre : 3,6,8
* Pseudo Life : (chaotique)
        - Survie : 2,3,8
        - Naitre : 3,5,7
* Replicator : (exponentiel)
        - Survie : 1,3,5,7
        - Naitre : 1,3,5,7
* Seeds : (exponentiel)
        - Survie : 
        - Naitre : 2
* Serviettes : (exponentiel)
        - Survie : 
        - Naitre : 2,3,4
* Stains : (stable)
        - Survie : 2,3,5,6,7,8
        - Naitre : 3,6,7,8
* Walled Cities : (stable)
        - Survie : 2,3,4,5
        - Naitre : 4,5,6,7,8
* Custom : (?)
        - Survie : ?
        - Naitre : ?


## Fonctionnalités

* Lancer une simulation
* Changer de règle du jeu
* Créer sa propre règle
* Sauvegarder sa propre règle de jeu dans un fichier.
* Charger sa ou ses règles de jeu
* Choisir le format d'enregistrement
* Sérialiser la grille en binaire [```GOL```](https://gitlab.iut-clermont.uca.fr/mapoulain1/game-of-life-javafx/wikis/home#format-gol) ou [```RLE```](https://gitlab.iut-clermont.uca.fr/mapoulain1/game-of-life-javafx/wikis/home#format-rle)
* Changer la taille de grille
* Changer l'état d'une cellule sur la grille en cliquant dessus
* Pouvoir remplir la grille de cellules de façon aléatoire
* Mettre en pause la simulation
* Reinitialiser la simulation à zéro
* Pouvoir choisir la vitesse de la simulation
* Pouvoir avancer itération par itération
* Pouvoir revenir en arrière dans la simulation
* Pouvoir prendre une capture d'écran de notre simulation
* Pouvoir dessiner sur la grille avec un pinceau de taille variable


## Taches


* ~~Faire l'interface de la page principale~~
    * ~~Faire les modules "Customize" et "Settings"~~
        - ~~Les modules comportent des options pour changer les paramètres de génération, les couleurs de la grille ..~~
* ~~Faire l'interface de la page de jeu~~
    * ~~Intégrer les modules "Customize" et "Settings"~~
    * ~~Intégrer les boutons PLAY/PAUSE, RESET, ALEA(int nombreCellEnVie), SAVE~~
    * ~~Intégrer les boutons zoom in/out~~
* ~~Faire le diagramme de classe~~
    * ~~Première version~~
    * ~~Version finale~~
* ~~Implémenter le diagramme de classe REV1~~
        - ~~Faire la grille hérité de canvas (couleur custom à choisir dans le menu principal)~~


## Installation

Option de la JVM : 

```
--module-path ${PATH_TO_JAVAFX} --add-modules=javafx.controls,javafx.fxml,javafx.media
```

Point d'entrée de l'application : 

```
src/app/Main.java
```

Testé avec Java 13


